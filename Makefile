CXXFLAGS := -g -Os
CXXCOMPILER := g++

all: interpreter

parser.c: lang.leg
	leg -v -o $@ $^

parser.o: parser.c
	$(CXXCOMPILER) -c -DYYDEBUG -o $@ $^

fail.o: fail.cpp
	$(CXXCOMPILER) -c -o $@ $^ $(CXXFLAGS)

nodes.o: nodes.cpp
	$(CXXCOMPILER) -c -o $@ $^ $(CXXFLAGS)

interpreter.o: interpreter.cpp
	$(CXXCOMPILER) -c -o $@ $^ $(CXXFLAGS)

eval.o: eval.cpp
	$(CXXCOMPILER) -c -o $@ $^ $(CXXFLAGS)

print.o: print.cpp
	$(CXXCOMPILER) -c -o $@ $^ $(CXXFLAGS)

interpreter: parser.o fail.o nodes.o interpreter.o eval.o print.o
	$(CXXCOMPILER) -o $@ $^ $(CXXFLAGS)

clean:
	rm *.o parser.c compiler
