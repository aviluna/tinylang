#pragma once

#include <stdio.h>

#ifdef DEBUG
#define PDEBUG(__fmtstr, ...) fprintf(stderr, __fmtstr "\n", __VA_ARGS__)
#else
#define PDEBUG(...)
#endif
