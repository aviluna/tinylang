#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "fail.h"
#include "debug.h"

#include "eval.h"

Scope * GLOBAL_SCOPE = NULL;
std::stack<Scope> SCOPE_STACK;

Variable add(Arglist * args){
	Variable accumulator = {0};

	if(!args->count)
		return accumulator; // return zero for null add

	for(unsigned index = 0;
		index < args->count;
		index++){
		accumulator.int32 += args->args[index]->eval().int32;
	}

	return accumulator;
}

Variable sub(Arglist * args){
	Variable accumulator = {0};

	if(args->count < 2)
		return accumulator; // return zero for null or unary sub

	accumulator = args->args[0]->eval();
	for(unsigned index = 1;
		index < args->count;
		index++){
		accumulator.int32 -= args->args[index]->eval().int32;
	}

	return accumulator;
}

Variable mul(Arglist * args){
	Variable accumulator = {0};

	if(!args->count)
		return accumulator; // return zero for null mul
	
	accumulator = args->args[0]->eval();
	if(args->count == 1)
		return accumulator; // return the value unmodified if unary

	for(unsigned index = 1;
		index < args->count;
		index++){
		accumulator.int32 *= args->args[index]->eval().int32;
	}

	return accumulator;
}

Variable div(Arglist * args){
	Variable accumulator = {0};

	if(!args->count)
		return accumulator; // return zero for null div
	
	accumulator = args->args[0]->eval();
	if(args->count == 1)
		return accumulator; // return the value unmodified if unary

	for(unsigned index = 1;
		index < args->count;
		index++){
		accumulator.int32 /= args->args[index]->eval().int32;
	}

	return accumulator;
}

Variable printhex(Arglist * args){
	Variable var = {0};

	if(!args->count){
		printf("\n");
		return var;
	}
	else{
		for(unsigned index = 0;
			index < args->count;
			index++){
			printf("0x%08X", args->args[index]->eval().int32);
			if(index != args->count - 1){ // if not the last loop
				// then print a space
				printf(" ");
			}
		}
		printf("\n");
		return var;
	}
}

Variable * findScope(std::string id){
	Scope::iterator iter = SCOPE_STACK.top().find(id);
	if(iter != SCOPE_STACK.top().end())
		return &(*iter).second; // found it in local scope!

	iter = GLOBAL_SCOPE->find(id);
	if(iter != GLOBAL_SCOPE->end()){
		PDEBUG("Warning: accessed %s at global scope\n",
				id.c_str());
		return &(*iter).second; // found it in global scope!
	}

	return NULL; // not found!
}

Variable * findScope(const char * c_str){
	std::string stringid(c_str);
	return findScope(stringid);
}

Variable Declaration::eval(){
	Variable var;

	if(strcmp(this->type, "int32")){
		fprintf(stderr,
				"error: type %s unimplemented; only int32 so far\n",
				this->type);
		exit(1);
	}

	Variable result = this->expression->eval();
	var.int32 = result.int32;

	std::string stringid(this->id);

	Variable * existing = findScope(stringid);
	if(existing){
		fprintf(stderr,
				"error: variable %s redefined;"
				"note tinylang doesn't allow shadowing globals\n",
				this->id);
		exit(1);
	}

	SCOPE_STACK.top().insert(std::pair<std::string, Variable>(stringid, var));

	return var;
}

Variable Assignment::eval(){
	Variable * varptr = findScope(id);
	if(!varptr){
		fprintf(stderr, "[ASSIGNMENT] Undefined variable `%s`\n",
				(const char*) this->id);
		fprintf(stderr, "Defined variables:\n");
		for(Scope::iterator begin = SCOPE_STACK.top().begin(),
							end = SCOPE_STACK.top().end();
			begin != end;
			begin++){
			fprintf(stderr, "%s = %i\n",
					begin->first.c_str(),
					begin->second.int32);
		}
		exit(1);
	}
	*varptr = this->expression->eval();
}

Variable Constant::eval(){
	Variable var;
	var.int32 = this->int32;
	return var;
}

Variable Funcall::eval(){
	if(!strcmp(this->id, "+") || !strcmp(this->id, "add")){
		return add(this->arglist);
	}
	else if(!strcmp(this->id, "-") || !strcmp(this->id, "sub")){
		return sub(this->arglist);
	}
	else if(!strcmp(this->id, "*") || !strcmp(this->id, "mul")){
		return mul(this->arglist);
	}
	else if(!strcmp(this->id, "/") || !strcmp(this->id, "div")){
		return div(this->arglist);
	}
	else if(!strcmp(this->id, "printhex")){
		return printhex(this->arglist);
	}
	else{
		fprintf(stderr, "Undefined function `%s`\n",
				this->id);
		exit(1);
	}
}

Variable Statementlist::eval(){
	Variable var;
	
	if(!GLOBAL_SCOPE){
		Scope global_scope;
		SCOPE_STACK.push(global_scope);
		GLOBAL_SCOPE = &SCOPE_STACK.top();
	}
	
	for(unsigned index = 0;
		index < this->count;
		index++){
		var = this->statements[index]->eval();
	}

	return var;
}

Variable IfStatement::eval(){
	Variable zero = {0};
	Variable condresult = this->cond->eval();
	if(condresult.uint32 != 0) // when more types are supported this should be the smallest size
		return this->body->eval();
	else{
		if(this->elsebody)
			return this->elsebody->eval();
		else
			return zero;
	}
}

Variable Node::eval(){
	Variable var = {0};
	Variable * varptr = NULL;
	switch(this->type){
		case NODE_NULL:
			printf("wtf, null node");
			return var;
			break;
		case NODE_DECLARATION:
			return ((Declaration *)this->data)->eval();
			break;
		case NODE_ASSIGNMENT:
			return ((Assignment *)this->data)->eval();
			break;
		case NODE_CONSTANT:
			return ((Constant *)this->data)->eval();
			break;
		case NODE_IDENTIFIER:
			varptr = findScope((const char*) this->data);
			if(!varptr){
				fprintf(stderr, "[NODE] Undefined variable `%s`\n",
					(const char*) this->data);
				exit(1);
			}
			else{
				var.int32 = varptr->int32;
				return var;
			}
			break;
		case NODE_FUNCALL:
			return ((Funcall *)this->data)->eval();
			break;
		case NODE_ARGLIST:
			//this... shouldn't happen
			printf("wtf, tried to evaluate an arglist!");
			return var;
			break;
		case NODE_IFSTATEMENT:
			return ((IfStatement *)this->data)->eval();
			break;
	}
	
	printf("wtf, unhandled Node->eval()");
	return var;
}

