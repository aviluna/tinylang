#pragma once

#define EVAL
#include "nodes.h"

#include <map>
#include <string>
#include <stack>
#include <stdint.h>

struct Variable{
	// the same data as (nodes.h)Constant, but without the same print()
	union{
		 int32_t	int32;
		uint32_t	uint32;
	};
};

typedef std::map<std::string, Variable> Scope;

extern Scope * GLOBAL_SCOPE;
extern std::stack<Scope> SCOPE_STACK;
