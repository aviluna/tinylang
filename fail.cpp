#include <stdio.h>
#include <stdlib.h>

#include "fail.h"

void ALLOC_FAIL_lower(const char * file, const unsigned line, const char * where){
	if(!where)
		where = "<UNSPECIFIED>";
	fprintf(stderr, "Allocation error in %s (%s:%u)\n", where, file, line);
	exit(2);
}
