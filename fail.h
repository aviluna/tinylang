#pragma once

void ALLOC_FAIL_lower(const char * file, const unsigned line, const char * where);

#define ALLOC_FAIL() ALLOC_FAIL_lower(__FILE__, __LINE__, __func__)

