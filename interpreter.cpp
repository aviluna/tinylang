#include <stdio.h>

#include "eval.h"
#include "nodes.h"

extern bool reachedEOF;
extern void yyerror(const char *message);
extern int yyparse();

int main(){
	while(yyparse() && !reachedEOF);

	if(reachedEOF){
		printf("Statements in global statement list:\n");
		Statementlist * statlist = (Statementlist *)GLOBAL_STATEMENT_LIST->data;
		statlist->print(0);
		printf("Evaluating!!!\n");
		statlist->eval();
	}
	else{
		yyerror("syntax error");
		return 1;
	}

	return 0;
}
