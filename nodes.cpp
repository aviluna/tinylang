#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include "fail.h"
#include "nodes.h"
#include "debug.h"

#include <vector>
#include <stack>

typedef std::vector<Node *> ARGLIST;
std::stack<ARGLIST> ARGLISTS_STACK;

typedef std::vector<Node *> STATEMENTLIST;
std::stack<STATEMENTLIST> STATEMENTLIST_STACK;

Node * GLOBAL_STATEMENT_LIST = NULL;

void ARGLIST_PRE(){
	ARGLISTS_STACK.push(ARGLIST());
}

void ARGLIST_ADD(Node * node){
	ARGLISTS_STACK.top().push_back(node);
}

void STATEMENTLIST_PRE(){
	STATEMENTLIST_STACK.push(STATEMENTLIST());
}

void STATEMENTLIST_ADD(Node * node){
	STATEMENTLIST_STACK.top().push_back(node);
}

/* ### C-CONSTRUCTORS ### */

Node * NewDeclaration(Node * ntype, Node * nid, Node * nexpr){
	PDEBUG("Making new Declaration: %s %s %s",
		   getNodeString(ntype),
		   getNodeString(nid),
		   getNodeString(nexpr));
	Node * newnode = (Node *)malloc(sizeof(Node));
	Declaration * newdecl = (Declaration *)malloc(sizeof(Declaration));

	if(!newnode || !newdecl)
		ALLOC_FAIL();

	newnode->type = NODE_DECLARATION;
	newnode->data = newdecl;

	// XXX consider doing type checking
	newdecl->type = (Identifier *)ntype->data;
	newdecl->id = (Identifier *)nid->data;
	newdecl->expression = nexpr;

	// delete the now-obsolete nodes (but keep their data!)
	free(ntype);
	free(nid);

	return newnode;
}

Node * NewAssignment(Node * nid, Node * nexpr){
	PDEBUG("Making new Assignment: %s %s",
		   getNodeString(nid),
		   getNodeString(nexpr));

	Node * newnode = (Node *)malloc(sizeof(Node));
	Assignment * newassn = (Assignment *)malloc(sizeof(Assignment));

	if(!newnode || !newassn)
		ALLOC_FAIL();

	newnode->type = NODE_ASSIGNMENT;
	newnode->data = newassn;

	newassn->id = ((Identifier *)nid->data);
	newassn->expression = nexpr;

	free(nid);

	return newnode;
}

Node * NewConstant(const char * string){
	PDEBUG("Making new Constant: %s",
		   string);
	
	Node * newnode = (Node *)malloc(sizeof(Node));
	Constant * newconst = (Constant *)malloc(sizeof(Constant));

	if(!newnode || !newconst)
		ALLOC_FAIL();
	
	newnode->type = NODE_CONSTANT;
	newnode->data = newconst;

	newconst->int32 = strtol(string, (char**)NULL, 0);

	return newnode;
}

Node * NewIdentifier(const char * string, const unsigned length){
	PDEBUG("Making new Identifier: %s",
			string);

	Node * newnode = (Node *)malloc(sizeof(Node));
	Identifier * newident = (Identifier *)malloc(sizeof(Identifier) * (length + 1));

	if(!newnode || !newident)
		ALLOC_FAIL();

	newnode->type = NODE_IDENTIFIER;
	newnode->data = newident;

	snprintf(newident, (length + 1), "%s", string);

	return newnode;
}

Node * NewFuncall(Node * nid, Node * narglist){
	PDEBUG("Making new Funcall: (%s id: %s) (%s count)",
			getNodeString(nid),
			nid->data,
			getNodeString(narglist));

	Node * newnode = (Node *)malloc(sizeof(Node));
	Funcall * newfunc = (Funcall *)malloc(sizeof(Funcall));

	if(!newnode || !newfunc)
		ALLOC_FAIL();

	newnode->type = NODE_FUNCALL;
	newnode->data = newfunc;

	newfunc->id = (Identifier *)nid->data;
	newfunc->arglist = (Arglist *)narglist->data;

	free(nid);
	free(narglist);

	return newnode;
}

Node * NewIfStatement(Node * cond, Node * body, Node * elsebody){
	PDEBUG("Making new IfStatement: (cond %s, body %s, elsebody %s)",
			getNodeString(cond),
			getNodeString(body),
			getNodeString(elsebody));

	Node * newnode = (Node *)malloc(sizeof(Node));
	IfStatement * newif = (IfStatement *)malloc(sizeof(IfStatement));

	if(!newnode || !newif)
		ALLOC_FAIL();

	newnode->type = NODE_IFSTATEMENT;
	newnode->data = newif;

	newif->cond = cond;
	newif->body = body;
	newif->elsebody = elsebody;

	return newnode;
}

Node * NewArglist(){
	PDEBUG("Making new Arglist", "");

	Node * newnode = (Node *)malloc(sizeof(Node));
	Arglist * newarglist = (Arglist *)malloc(sizeof(Arglist));
	if(!newnode)
		ALLOC_FAIL();
	if(!newarglist)
		ALLOC_FAIL();

	newnode->type = NODE_ARGLIST;
	newnode->data = newarglist;
	
	newarglist->count = ARGLISTS_STACK.top().size();
	newarglist->args = (Node **)malloc(sizeof(Node *) * newarglist->count);

	for(unsigned index = 0;
		index < newarglist->count;
		index++){
		newarglist->args[index] = ARGLISTS_STACK.top()[index];
	}

	ARGLISTS_STACK.pop();

	return newnode;
}

Node * NewStatementlist(){
	PDEBUG("Making new Statmentlist", "");

	Node * newnode = (Node *)malloc(sizeof(Node));
	Statementlist * newstatlist = (Statementlist *)malloc(sizeof(Statementlist));
	if(!newnode)
		ALLOC_FAIL();
	if(!newstatlist)
		ALLOC_FAIL();

	newnode->type = NODE_STATLIST;
	newnode->data = newstatlist;
	
	newstatlist->count = STATEMENTLIST_STACK.top().size();
	newstatlist->statements = (Node **)malloc(sizeof(Node *) * newstatlist->count);

	for(unsigned index = 0;
		index < newstatlist->count;
		index++){
		newstatlist->statements[index] = STATEMENTLIST_STACK.top()[index];
	}

	STATEMENTLIST_STACK.pop();

	return newnode;
}

/* MISC */

const char * NODE_TYPE_STRINGS[]={
	"NODE_NULL",
	"NODE_DECLARATION",
	"NODE_ASSIGNMENT",
	"NODE_CONSTANT",
	"NODE_IDENTIFIER",
	"NODE_FUNCALL",
	"NODE_ARGLIST",
	"NODE_STATLIST"
};

const char * getNodeString(Node * node){
	if(!node)
		return "(NULL ptr)";
	else if(node->type >= NODE_INVALID)
		return "(Invalid node type!)";
	else
		return NODE_TYPE_STRINGS[node->type];
}
