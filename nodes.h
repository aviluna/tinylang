#pragma once

#include <stdint.h>

#ifdef EVAL
struct Variable;
#include "eval.h"
#endif

struct LinkedList;
struct Node;

enum NodeTypes{
	NODE_NULL,
	NODE_DECLARATION,
	NODE_ASSIGNMENT,
	NODE_CONSTANT,
	NODE_IDENTIFIER,
	NODE_FUNCALL,
	NODE_ARGLIST,
	NODE_STATLIST,
	NODE_IFSTATEMENT,
	NODE_INVALID
};

struct Declaration;
struct Assignment;
struct Constant;
typedef char Identifier;
struct Funcall;
struct Arglist;

struct Node{
	enum NodeTypes	type;
	void *			data;

	void print(unsigned indent_level);

#ifdef EVAL
	Variable eval();
#endif
};

struct Declaration{
	Identifier *	type;
	Identifier *	id;
	Node *			expression;
	
	void print(unsigned indent_level);

#ifdef EVAL
	Variable eval();
#endif
};

struct Assignment{
	Identifier *	id;
	Node *			expression;
	
	void print(unsigned indent_level);

#ifdef EVAL
	Variable eval();
#endif
};

struct Constant{
	union{
		 int32_t	int32;
		uint32_t	uint32;
	};
	
	void print(unsigned indent_level);
#ifdef EVAL
	Variable eval();
#endif
};

struct Funcall{
	Identifier *	id;
	Arglist	*		arglist;
	
	void print(unsigned indent_level);
#ifdef EVAL
	Variable eval();
#endif
};

struct Arglist{
	unsigned		count;
	Node **			args;

	void print(unsigned indent_level);
#ifdef EVAL
	Variable eval();
#endif
};

struct Statementlist{
	unsigned		count;
	Node **			statements;

	void print(unsigned indent_level);
#ifdef EVAL
	Variable eval();
#endif
};

struct IfStatement{
	Node *			cond;
	Node *			body;
	Node *			elsebody;

	void print(unsigned indent_level);
#ifdef EVAL
	Variable eval();
#endif
};

void ARGLIST_PRE();
void ARGLIST_ADD(Node * node);

void STATEMENTLIST_PRE();
void STATEMENTLIST_ADD(Node * node);

extern Node * GLOBAL_STATEMENT_LIST;

Node * NewDeclaration(Node * ntype, Node * nid, Node * nexpr);
Node * NewAssignment(Node * nid, Node * nexpr);
Node * NewConstant(const char * string);
Node * NewIdentifier(const char * string, unsigned length);
Node * NewFuncall(Node * nid, Node * narglist);
Node * NewIfStatement(Node * cond, Node * body, Node * elsebody);
Node * NewArglist();
Node * NewStatementlist();

const char * getNodeString(Node * node);
