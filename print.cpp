#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fail.h"
#include "nodes.h"

char * makeIndentString(unsigned indent_level);

char * makeIndentString(unsigned indent_level){
	char * indent_string = (char *)malloc(sizeof(char) * (indent_level + 1));
	if(!indent_string)
		ALLOC_FAIL();
	memset(indent_string, ' ', indent_level);
	indent_string[indent_level] = 0;
	if(indent_level > 0)
		indent_string[indent_level - 1] = '|';

	return indent_string;
}

void Declaration::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Declaration\n"
		   "%s- Type: %s\n"
		   "%s- Id:   %s\n"
		   "%s- Expr:\n",
		   indent_string,
		   indent_string, this->type,
		   indent_string, this->id,
		   indent_string);
	free(indent_string);
	
	this->expression->print(indent_level + 2);
}

void Assignment::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Assignment\n"
		   "%s- Id:   %s\n"
		   "%s- Expr:\n",
		   indent_string,
		   indent_string, this->id,
		   indent_string);
	free(indent_string);

	this->expression->print(indent_level + 2);
}

void Constant::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Constant\n"
		   "%s- Type:  int32\n"
		   "%s- Value: %i\n",
		   indent_string,
		   indent_string,
		   indent_string, this->int32);
	
	free(indent_string);
}

void Funcall::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Funcall\n"
		   "%s- Id:    %s\n"
		   "%s-- Args:\n",
		   indent_string,
		   indent_string, this->id,
		   indent_string);
	free(indent_string);

	this->arglist->print(indent_level + 2);
}

void Arglist::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Arglist\n"
		   "%s- Count: %u\n"
		   "%s-- Args:\n",
		   indent_string,
		   indent_string, this->count,
		   indent_string);

	for(unsigned index = 0;
		index < this->count;
		index++){
		this->args[index]->print(indent_level + 2);
	}

	free(indent_string);
}

void Statementlist::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);
	printf("%s Statementlist\n"
		   "%s- Count: %u\n"
		   "%s-- Statements:\n",
		   indent_string,
		   indent_string, this->count,
		   indent_string);

	for(unsigned index = 0;
		index < this->count;
		index++){
		this->statements[index]->print(indent_level + 2);
	}

	free(indent_string);
}

void Node::print(unsigned indent_level){
	char * indent_string = makeIndentString(indent_level);

	switch(this->type){
		case NODE_NULL:
			printf("wtf, null node");
			break;
		case NODE_DECLARATION:
			((Declaration *)this->data)->print(indent_level + 1);
			break;
		case NODE_ASSIGNMENT:
			((Assignment *)this->data)->print(indent_level + 1);
			break;
		case NODE_CONSTANT:
			((Constant *)this->data)->print(indent_level + 1);
			break;
		case NODE_IDENTIFIER:
			printf("%s- Identifier: %s\n", indent_string, ((Identifier*) this->data));
			break;
		case NODE_FUNCALL:
			((Funcall *)this->data)->print(indent_level + 1);
			break;
		case NODE_ARGLIST:
			((Arglist *)this->data)->print(indent_level + 1);
			break;
	}

	free(indent_string);
}


